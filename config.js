var dotenv = require('dotenv');
var cfg = {};

if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'test') {
  dotenv.config({path: '.env'});
} else {
  dotenv.config({path: '.env.test', silent: true});
}

// Example
cfg.example = process.env.EXAMPLE || "This is an example";

var requiredConfig = [cfg.example];
var isConfigured = requiredConfig.every(function(confValue) {
  return confValue || false;
});

if (!isConfigured) {
  var errorMessage =
      'EAMPLE must be set!';
  throw new Error(errorMessage);
}

// Export configuration object
module.exports = cfg;
